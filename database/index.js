const mysql = require('mysql');

// THIS IS THE UNIQUE FUNCTION TO CONNECT AND SEND QUERIES TO DB
const query = (sql) => {

    return new Promise((resolve, reject) => {

        const {
            MYSQL_HOST,
            MYSQL_PORT,
            MYSQL_USER,
            MYSQL_PASSWORD,
            MYSQL_DBNAME
        } = process.env

        const con = mysql.createConnection({
            host: MYSQL_HOST,
            port : MYSQL_PORT,
            user: MYSQL_USER,
            password: MYSQL_PASSWORD,
            database: MYSQL_DBNAME
        })
    
        con.connect((err) => {
        
            if (err) throw console.error(err)
    
            con.query(sql, function (err, result) {
                if (err) throw reject(err);

                console.log("Query finished")
                resolve(result)
    
                
            })
    
        })

    })

}

module.exports = {
    query
}