const database = require('../database')

class Stats {

    // GET DATA FROM DB
    selectFromDB(){

        const query = `SELECT * FROM teamknology.mutaciones`

        return new Promise((resolve, reject) => {

            database.query(query).then(res => {
                resolve(res)
            }).catch(err => {
                console.error(err)
                reject(err)
            })

        })

    }

    // CALCULATE THE RATIO
    calculateRatio(a,b){
        
        return parseFloat((a/b).toFixed(1))

    }

    // PREPARE THE RESPONSE OBJECT TO SEND TO ENDPOINT
    async prepareResponse(){

        const result = await this.selectFromDB().then(res => {

            var result = {
                count_mutations : 0,
                count_no_mutation : 0,
                ratio : 0
            }

            var count_mutations = 0
            var count_no_mutation = 0

            res.map(v => {

                const {muted} = v

                if(muted){
                    count_mutations++
                }else{
                    count_no_mutation++
                }

            })

            result = {
                count_mutations,
                count_no_mutation,
                ratio : this.calculateRatio(count_mutations, count_no_mutation)
            }

            return result

        }).catch(err => {

            console.log('---------------------------------rr---------------------------------');
            console.log(err)
            console.log('---------------------------------rr---------------------------------');

        })
        
        return result

    }

    // INIT PROCESS
    async init(){

        const result = await this.prepareResponse()

        return result

    }

}

module.exports = {
    Stats
}