const database = require('../database')

class Mutation{

    string = ''
    limit = 0
    verticalArray = []
    resultVertical = false
    diagonalArray = []
    resultDiagonal = false
    helpV = 0
    matriz = []
    mutations = 0
    result = {
        error : true,
        status : 403,
        hasMutation : false,
        message : 'No existe mutación.'
    }

    // CHECK IF THE LETTER IS VALID
    validateLetter(array){

        for (let i = 0; i < array.length; i++) {

            const word = array[i];

            for (let a = 0; a < word.length; a++) {

                const letter = word[a].toUpperCase();

                if( ['A','C','T','G'].indexOf(letter) === -1){
                    return false
                }
                
            }
            
        }

        return true

    }

    // GET AN ARRAY AND CONVERT IN A MATRIX
    convertToMatriz(array){

        for (let i = 0; i < array.length; i++) {

            const element = array[i]
            
            this.matriz.push([])

            for (let a = 0; a < element.length; a++) {

                const letter = element[a]

                this.matriz[i].push(letter)
                
            }

        }

    }

    // SEARCH IN HORIZONTAL MUTATIONS
    checkHorizontal(array){

        var result = false

        for (let i = 0; i < array.length; i++) {

            this.string = array[i]

            if(this.checkMutation()){
                this.mutations++
                result = true
            }            
            
        }

        return result

    }

    // SEARCH IN VERTICAL MUTATIONS
    checkVertical(){

        for (let i = 0; i < this.matriz.length; i++) {

            // TOMARE TODAS LAS POSICIONES 0 DEL ROW
            const row = this.matriz[i];

            for (let a = 0; a < row.length; a++) {

                if(a === this.helpV){

                    this.string += row[this.helpV]

                }
                
            }

            if(i === this.limit){

                this.helpV++
            
                this.verticalArray.push(this.string)

                this.string = ''

                if(this.helpV <= this.limit){

                    this.checkVertical(this.limit)

                }else{
                    // FINISH, CHECK MUTATION

                    const resultado = this.verticalArray.filter(v => {

                        this.string = v

                        if(this.checkMutation()){
                            return true
                        }

                    })

                    if(resultado.length > 0){

                        resultado.map(v => {
                            this.mutations++
                        })
                        
                        this.resultVertical = true

                    }

                }
            }
            
        }

        return this.resultVertical

    }

    // SEARCH IN DIAGONAL MUTATIONS
    checkDiagonal(){

        var DDerecha = ''
        var DIzquierda = ''

        for (let i = 0; i < this.matriz.length; i++) {

            // TOMARE TODAS LAS POSICIONES 0 DEL ROW
            const row = this.matriz[i];

            for (let a = 0; a < row.length; a++) {

                if(a === 0){
                    // CHECK DIAGONAL A DERECHA
                    DDerecha += row[i]
                    DIzquierda += row[(row.length - 1) - i]

                }            
                
            }

            if(i === this.limit){
                
                this.diagonalArray.push(DDerecha)
                this.diagonalArray.push(DIzquierda)

                const resultado = this.diagonalArray.filter(v => {

                    this.string = v

                    if(this.checkMutation()){
                        return true
                    }

                })

                if(resultado.length > 0){

                    resultado.map(v => {
                        this.mutations++
                    })
                    
                    this.resultDiagonal = true

                }

            }
            
        }

        return this.resultDiagonal

    }

    // CHECK A STRING IF HAS MUTATIONS
    checkMutation(){

        if(/(.)\1{3,}/.test(this.string)){
            this.string = ''
            return true
        }else{
            this.string = ''
            return false
        }

    }

    // SAVE DATA IN DB AFTER CALCULATED
    saveRegisterInDB(muted){

        database.query(`INSERT INTO teamknology.mutaciones (muted) VALUES (${muted})`).then(res => {
            console.log(res)
        }).catch(err => {
            console.error(err)
        })

    }

    sendSuccess(message){
        this.result = {
            ...this.result,
            error : false,
            message : message
        }
    }

    // INIT PROCESS
    init(array){

        var message = 'Se detectó mutación en:'

        // CHECK IF VALID
        if(!this.validateLetter(array)){
            message = 'Las bases nitrogenadas no son válidas'
            this.result.message = message
            return this.result
        }

        // CONVERT TO UPPERCASE
        array = array.map(v => {
            return v.toUpperCase()
        })

        this.limit = array.length - 1

        // CHECK HORIZONTAL
        if (this.checkHorizontal(array)){

            message += ' HORIZONTAL '

            this.sendSuccess(message)
            
            console.log("es horizontal")
            
        }

        this.convertToMatriz(array)

        // CHECK VERTICAL
        if(this.checkVertical()){

            message += ' VERTICAL '

            this.sendSuccess(message)

            console.log("es vertical")

        }

        // CHECK DIAGONAL
        if(this.checkDiagonal()){

            message += ' DIAGONAL '

            this.sendSuccess(message)

            console.log("es diagonal")

        }

        if(this.result.error){
            // SAVE IN DB
            this.saveRegisterInDB(0)
        }else{
            if(this.mutations >= 2){
                this.saveRegisterInDB(1)
                this.result = {
                    ...this.result,
                    hasMutation : true,
                    status : 200
                }
            }else{
                this.saveRegisterInDB(0)
            }
        }

        console.log('---------------------------------this.mutations---------------------------------');
        console.log(this.mutations)
        console.log('---------------------------------this.mutations---------------------------------');
        
        
        return this.result

    }

}

module.exports = {
    Mutation
}