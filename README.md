# Prueba Backend Teamknowlogy - App Engine
 - Versión de Node en ambiente local: 12.0.0
 - Vesrion de Node en GCloud: 12.0.0

### Instalación Local
 - Clonar el repositorio de [GIT](https://bitbucket.org/thecodingbear/prueba-teamknowlogy-app-engine/src/master/) en la computadora local.
 - Ejecutar el comando "npm install" en la carpeta raíz del proyecto.
 - Para correr el proyecto de forma local ejecutar el comando "npm start".
 - El servidor local quedará corriendo en http://localhost:8080.
 - Podrá ver el ratio de Mutaciones en GET http://localhost:8080/api/stats
 - Podrá enviar cadenas de mutaciones por POST http://localhost:8080/api/mutation enviando como JSON dicha cadena

Nota: Tanto de forma local como en la propia nube, ambas versiones están conectadas a la base de datos MySQL de la propia App Engine en la Cloud de Google.
 
Formato de cadena json:
```
{
    "dna2":[
        "ATGCGA",
        "CAGTGC",
        "TTATGT",
        "AGAAGG",
        "CCCCA",
        "TCACTG"
    ]
}
```

### Información de la Cloud
 - Host, Base de api rest : https://prueba-teamknowlogy-281322.uc.r.appspot.com/api
 - POST : /mutation
 (Comprueba una cadena de ADN y la guarda en base de datos)
```
Header:
"Content-Type", "application/json"
Body Json:
{
    "dna":[
        "ATGCGA",
        "CAGTGC",
        "TTATGT",
        "AGAAGG",
        "CCCCA",
        "TCACTG"
    ]
}
```
 - GET : /stats
 (Obtienes información del ratio de todas las cadenas ingresadas)