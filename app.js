// GET ENV VARIABLES
require('dotenv').config({path: __dirname + '/.env'})
const express = require('express');
const bodyParser = require('body-parser')
// MUTATIONS ALGORITMS
const Mutation = require('./mutation')
// GET STATS
const Stats = require('./mutation/stats')

const app = express();
app.use(bodyParser.json());

// GET STATS FROM DB
app.get('/api/stats', (req, res) => {

  const stats = new Stats.Stats()

  stats.init().then(json => {

    res.status(200).json(json).end();

  })

})

// CALCULATE ALGORIT AND SAVE DATA IN DB
app.post('/api/mutation', (req, res) => {

  const dna = req.body.dna

  if(dna){
    const mutation = new Mutation.Mutation()

    const muta = mutation.init(dna)
    const {status} = muta
  
    if(muta){
      res.status(status).send(muta).end();
    }else{
      res.status(status).send(muta).end();
    }
  }else{
    res.status(400).send('Es necesario enviar el parámetro "dna".').end();
  }

});

// Start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});
// [END gae_node_request_example]

module.exports = app;
